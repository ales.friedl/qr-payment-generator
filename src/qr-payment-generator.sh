#!/bin/bash

SCRIPT_DIR=$(dirname "$(readlink -f "$0")")
HAS_MAPFILE_D="$(mapfile -d "" <<<"" && echo "true")"

declare -A RETVAL_ARR

default_options() {
	help=false
	usage=false

	verbose=false
	debug=false

	#config="qr-code-generator.conf" # default unset

	#account="" # default unset
	amount=""
	currency_code="CZK"
	sender_reference=""
	due_date=$(date "+%Y%m%d")
	variable_symbol=""
	message=""
	recipient_name=""

	crc32=true
	format="ANSI256UTF8"

	# default qr-payment-generator configuration
	iban_country_code="CZ"
	dry_run=false
	default_global_config="$SCRIPT_DIR/qr-payment-generator.conf"
	default_local_config="qr-payment-generator.conf"
}

parse_config() {
	local configuration_file="$1"
	# shellcheck source=qr-payment-generator.conf
	if [[ -s "$configuration_file" ]]; then
		. "$configuration_file"
	fi
}

parse_options() {
	options=$#
	opts=""
	leave=false

	while [[ $# -gt 0 && "$leave" == "false" ]]; do
		case "$1" in
			-h|--help)
				help=true
				;;
			-v|--verbose)
				verbose=true
				;;
			--no-verbose)
				verbose=false
				;;
			--debug)
				debug=true
				;;
			--no-debug)
				debug=false
				;;
			-c|--config|--config=*)
				if [[ "$1" =~ ^-.*=.*$ ]]; then config=${1#*=};
				else opts="$opts $1"; shift; config="$1"; fi
				if [[ ! -s "$config" ]]; then
					printf "error: configuration file %s not found (must be accessible and non-empty)" "$config" >&2
					exit 1
				fi
				parse_config "$config"
				;;
			--acc|--acc=*|--account|--account=*)
				if [[ "$1" =~ ^-.*=.*$ ]]; then account=${1#*=};
				else opts="$opts $1"; shift; account="$1"; fi
				;;
			--am|--am=*|--amount|--amount=*)
				if [[ "$1" =~ ^-.*=.*$ ]]; then amount=${1#*=};
				else opts="$opts $1"; shift; amount="$1"; fi
				;;
			--cc|--cc=*|--currency_code|--currency_code=*)
				if [[ "$1" =~ ^-.*=.*$ ]]; then currency_code=${1#*=};
				else opts="$opts $1"; shift; currency_code="$1"; fi
				;;
			--rf|--rf=*|--sender-reference|--sender-reference=*)
				if [[ "$1" =~ ^-.*=.*$ ]]; then sender_reference=${1#*=};
				else opts="$opts $1"; shift; sender_reference="$1"; fi
				;;
			--dt|--dt=*|--due-date|--due-date=*)
				if [[ "$1" =~ ^-.*=.*$ ]]; then due_date=${1#*=};
				else opts="$opts $1"; shift; due_date="$1"; fi
				;;
			--x-vs|--x-vs=*|--variable-symbol|--variable-symbol=*)
				if [[ "$1" =~ ^-.*=.*$ ]]; then variable_symbol=${1#*=};
				else opts="$opts $1"; shift; variable_symbol="$1"; fi
				;;
			--msg|--msg=*|--message|--message=*)
				if [[ "$1" =~ ^-.*=.*$ ]]; then message=${1#*=};
				else opts="$opts $1"; shift; message="$1"; fi
				;;
			--rn|--rn=*|--recipient-name|--recipient-name=*)
				if [[ "$1" =~ ^-.*=.*$ ]]; then recipient_name=${1#*=};
				else opts="$opts $1"; shift; recipient_name="$1"; fi
				;;
			--crc32)
				crc32=true
				;;
			--no-crc32)
				crc32=false
				;;
			-f|--format|--format=*)
				if [[ "$1" =~ ^-.*=.*$ ]]; then format=${1#*=};
				else opts="$opts $1"; shift; format="$1"; fi
				;;
			--)
				leave=true
				;;
			-*)
				usage=true
				;;
			*)
				leave=true
				;;
		esac
		if ! $leave; then opts="$opts $1"; fi
		if ! $leave; then shift; fi
	done;
	to_shift=$((options - $#))
}

process_options() {
	if $usage; then
		usage
		exit 1;
	fi

	if $help; then
		usage
		exit 0;
	fi
}

options() {
	default_options
	parse_config "$default_global_config"
	parse_config "$default_local_config"
	parse_options "$@"
	process_options
}

usage() {
	# format="PNG,PNG32,EPS,SVG,XPM,ANSI,ANSI256,ASCII,ASCIIi,UTF8,UTF8i,ANSIUTF8,ANSIUTF8i,ANSI256UTF8"
	printf "Usage: %s\\n" "$0"
}

declare -A spd_asoc_arr

print_r() {
	local -n arr=$1
	local -r arr_name=$1
	for key in "${!arr[@]}"; do
		printf "%s[\"%s\"]=\"%s\"\\n" "$arr_name" "$key" "${arr[$key]}"
	done
}

parse_bban() {
	local -r bban="$1"
	[[ "$bban" =~ ^(([0-9]{0,6})-)?([0-9]{1,10})/([0-9]{4})$ ]]
	RETVAL_ARR["account_prefix"]=${BASH_REMATCH[2]}
	RETVAL_ARR["account_number"]=${BASH_REMATCH[3]}
	RETVAL_ARR["bank_code"]=${BASH_REMATCH[4]}
}

normalize_bban() {
	local -r bban="$1"
	parse_bban "$bban"
	local -r normalized_bban=$(printf "%06.0f%s%010.0f/%04.0f" "${RETVAL_ARR["account_prefix"]}" "${RETVAL_ARR["account_prefix"]:+-}" "${RETVAL_ARR["account_number"]}" "${RETVAL_ARR["bank_code"]}")
	RETVAL="$normalized_bban"
}

normalize_iban() {
	local -r iban="$1"
	local -r iban_without_spaces=${iban// }
	[[ "$iban_without_spaces" =~ ^([A-Z0-9]{0,34})(\+([A-Z0-9]{8,11}))?$ ]]
	local -r normalized_iban=${BASH_REMATCH[1]}
	#local -r bic=${BASH_REMATCH[3]}
	RETVAL="$normalized_iban"
}

convert_bban_to_iban() {
	local -r bban="$1"
	normalize_bban "$bban"
	local -r normalized_bban="$RETVAL"
	local -r account_number=${normalized_bban%%/*}
	local -r bank_code=${normalized_bban##*/}
	#echo "account number: $account_number"
	#echo "bank code: $bank_code"
	local -r iban_with_blank_checksum="${iban_country_code}00${bank_code}${account_number/-/}"
	calculate_checksum_of_iban "$iban_with_blank_checksum"
	local -r checksum="$RETVAL"
	local -r iban="${iban_country_code}${checksum}${bank_code}${account_number/-/}"
	RETVAL="$iban"
}

parse_iban() {
	local -r iban="$1"
	normalize_iban "$iban"
	local -r normalized_iban=$RETVAL
	local -r cc=${normalized_iban:0:2}
	local -r checksum=${normalized_iban:2:2}
	local -r base=${normalized_iban:4}
	#echo "IBAN Country Code: $cc"
	#echo "IBAN checksum: $checksum"
	#echo "IBAN base: ${base}"
	RETVAL_ARR["cc"]="${cc}"
	RETVAL_ARR["checksum"]="${checksum}"
	RETVAL_ARR["base"]="${base}"
}

calculate_checksum_of_iban() {
	local -r iban="$1"
	normalize_iban "$iban"
	local -r normalized_iban=$RETVAL

	[[ ${#normalized_iban} -lt 5 ]] && exit 1
	parse_iban "$normalized_iban"
	local value="${RETVAL_ARR["base"]}${RETVAL_ARR["cc"]}00"
	abc="ABCDEFGHIJKLMNOPQRSTUVWXYZ"
	for idx in $(seq 1 ${#abc}); do
		#echo "${abc:$((idx-1)):1} -> $((idx+9))"
		value=${value//${abc:$((idx-1)):1}/$((idx+9))}
	done
	#echo "IBAN value: ${value}"
	local -r checksum=$(bc -l <<<"scale=0; checksum = 98 - $value % 97; if (checksum < 10) print 0; checksum")
	#echo "IBAN checksum: ${checksum}"

	RETVAL="$checksum"
}

is_iban_valid() {
	local -r iban="$1"
	normalize_iban "$iban"
	local -r normalized_iban=$RETVAL
	local -r iban_length_max="34" #24
	if [[ "${#normalized_iban}" -gt "$iban_length_max" ]]; then
		printf "IBAN length exceeds allowed \"%s\" characters\\n" "$iban_length_max" >&2
		return 1
	fi

	parse_iban "$normalized_iban"
	local -r checksum=${RETVAL_ARR["checksum"]}

	calculate_checksum_of_iban "$normalized_iban"
	local -r calculated_checksum="$RETVAL"

	if [[ "$calculated_checksum" != "$checksum" ]]; then
		printf "IBAN checksum invalid: %s\\n" "$normalized_iban" >&2
		printf "                         ^^\\n" >&2
		printf "IBAN checksum invalid: calculated %s != %s\\n" "$calculated_checksum" "$checksum" >&2
		return 1
	fi

	if $debug; then
		printf "IBAN checksum: VALID\\n"
	fi
}

is_valid() {
	local -r value="$1"
	local -r allowed_characters="$2"
	local -r length_max="$3"

	if [[ "${#value}" -gt "$length_max" ]]; then
		printf "value too long (%s > %s, value=\"%s\")\\n" "${#value}" "$length_max" "$value" >&2
		return 1
	fi
	if [[ -n "$value" && ! "$value" =~ $allowed_characters ]]; then
		printf "value ($value) includes invalid character (not in %s)\\n" "$allowed_characters" >&2
		return 1
	fi
}

convert_account_to_iban_and_check_is_valid() {
	local -r account="$1"
	if [[ "$account"  =~ .*/.* ]]; then
		if $debug; then
			printf "Account number recognized as BBAN. Converting to IBAN...\\n"
		fi
		convert_bban_to_iban "$account"
		# RETVAL="$RETVAL"
	else
		if $debug; then
			printf "Account number recognized as IBAN. Checking for validity...\\n"
		fi
		normalize_iban "$account"
		local -r normalized_iban=$RETVAL
		is_iban_valid "$normalized_iban" || exit 1
		RETVAL="$normalized_iban"
	fi
}

if_not_set_ask() {
	local -r prompt=$1
	local -r -n var=$2
	[[ -z "${var+x}" ]] && read -p "$prompt" -r var
}

if_not_empty_add() {
	local -r -n var=$1
	local -r key=$2
	local -r value=$3
	[[ -n "$value" ]] && var+=(["$key"]="$value")
}

save_configuration_to_file() {
	local -r ts=$(date "+%Y%m%d%H%M")
	cat > "qr-payment.$ts.conf" <<-EOF
account="$account"
amount="$amount"
currency_code="$currency_code"
due_date="$due_date"
message="$message"
recipient_name="$recipient_name"
sender_reference="$sender_reference"
variable_symbol="$variable_symbol"
EOF
}

#

options "$@"
if [[ "$to_shift" -gt 0 ]]; then shift $to_shift; to_shift=0; fi


if_not_set_ask "BBAN or IBAN: " account

convert_account_to_iban_and_check_is_valid "$account"
account_iban="$RETVAL"

if_not_set_ask "Amount: " amount

is_valid "$amount" "^[1-9][0-9]*(\.[0-9][0-9])?$" "10" || exit 1
is_valid "$due_date" "^[0-9]{8}$" "8" || exit 1
is_valid "$message" "^[^*]{0,60}$" "60" || exit 1
#is_valid "$message" "0-9A-Z%*+-./:" "60" || exit 1
is_valid "$recipient_name" "^[^*]{0,25}$" "25" || exit 1
is_valid "$sender_reference" "^[0-9]{0,16}$" "16" || exit 1

save_configuration_to_file

# generate qr

short_payment_descriptor="SPD"
protocol_version="1.0"

spd_asoc_arr=(
# obligatory keys
["ACC"]="$account_iban"
)

# optional keys
if_not_empty_add spd_asoc_arr "AM" "$amount"
if_not_empty_add spd_asoc_arr "CC" "$currency_code"
if_not_empty_add spd_asoc_arr "RF" "$sender_reference"
if_not_empty_add spd_asoc_arr "DT" "$due_date"
if_not_empty_add spd_asoc_arr "X-VS" "$variable_symbol"
if_not_empty_add spd_asoc_arr "MSG" "$message"
if_not_empty_add spd_asoc_arr "RN" "$recipient_name"

if $debug; then
	printf "SPD Associative Array:\\n"
	print_r spd_asoc_arr
fi

if [[ "$HAS_MAPFILE_D" == "true" ]]; then
	mapfile -d '' spd_asoc_arr_sorted_keys < <(printf '%s\0' "${!spd_asoc_arr[@]}" | sort -z)
else
	# $BASH_VERSION < 4.4
	declare -a spd_asoc_arr_sorted_keys
	while IFS=$'\0' read -r key; do
		spd_asoc_arr_sorted_keys+=("$key")
	done < <(printf '%s\n' "${!spd_asoc_arr[@]}" | sort)
	# OR
	#local spd_asoc_arr_sorted_keys=($(printf '%s\n' "${!a[@]}" | sort))
fi

spd_arr=(
"$short_payment_descriptor"
"$protocol_version"
)
for key in "${spd_asoc_arr_sorted_keys[@]}"; do
	spd_arr+=("$key:${spd_asoc_arr[$key]}")
done

if $debug; then
	printf "SPD Array:\\n"
	print_r spd_arr
fi

spd_canonic=$(IFS="*" ; echo "${spd_arr[*]}")

if $debug; then
	printf "Canonic SPD:\\n%s\\n" "$spd_canonic"
fi

spd="$spd_canonic"
if $crc32; then
	crc32_value=$(printf "%s" "$spd_canonic" | gzip -c | tail -c8 | hexdump -n4 -e '"%08X"')
	if $debug; then
		printf "Calculated CRC32:\\n%s\\n" "$crc32_value"
	fi
	spd+="*CRC32:$crc32_value"
fi

if $debug; then
	printf "Final SPD:\\n%s\\n" "$spd"
fi
if ! $debug && $verbose; then
	printf "%s\\n" "$spd"
fi

if ! $dry_run; then
	qrencode -t "$format" -o - "$spd"
fi
