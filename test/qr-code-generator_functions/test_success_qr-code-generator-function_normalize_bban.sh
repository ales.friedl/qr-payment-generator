test_success_qr-payment-generator_function_normalize_bban() {
	# shellcheck disable=SC1090
	. "$PROJECT_ROOT/bin/qr-payment-generator.example.conf"
	# shellcheck disable=SC1090
	. "$PROJECT_ROOT/bin/qr-payment-generator" </dev/null

	declare -A data
	data=(
		["19-123457/0710"]="000019-0000123457/0710"
	)
	for value in "${!data[@]}"; do
		printf "Trying \"%s\"...\\n" "$value"
		normalize_bban "$value"
		assert_equal "$RETVAL" "${data[$value]}" || return 1
	done
}
