test_success_qr-payment-generator_function_convert_bban_to_iban() {
	# shellcheck disable=SC1090
	. "$PROJECT_ROOT/bin/qr-payment-generator.example.conf"
	# shellcheck disable=SC1090
	. "$PROJECT_ROOT/bin/qr-payment-generator" </dev/null

	declare -A data
	data=(
		["000019-0000123457/0710"]="CZ3507100000190000123457"
	)
	for value in "${!data[@]}"; do
		printf "Trying \"%s\"...\\n" "$value"
		convert_bban_to_iban "$value"
		assert_equal "$RETVAL" "${data[$value]}" || return 1
	done
}
