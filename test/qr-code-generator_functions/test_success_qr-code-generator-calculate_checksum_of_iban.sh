test_success_qr-payment-generator_function_calculate_checksum_of_iban() {
	# shellcheck disable=SC1090
	. "$PROJECT_ROOT/bin/qr-payment-generator.example.conf"
	# shellcheck disable=SC1090
	. "$PROJECT_ROOT/bin/qr-payment-generator" </dev/null

	declare -A data
	data=(
		["CZ9907100000190000123457"]="35"
	)
	for value in "${!data[@]}"; do
		printf "Trying \"%s\"...\\n" "$value"
		calculate_checksum_of_iban "$value"
		assert_equal "$RETVAL" "${data[$value]}" || return 1
	done
}
