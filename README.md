# qr-payment-generator

QR payment generator in Bash using qrencode

## Usage

`qr-payment.conf:`

```
# payment details:

account="000000-0123456789/0123"
#account="CZ2501230000000123456789"
amount="123"
currency_code="CZK"
variable_symbol=""
message="message"

# default script configuration:
# iban_country_code="CZ"
# debug=false
```

```bash
bash$ ./qr-payment
```
